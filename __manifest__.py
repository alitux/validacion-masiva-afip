# -*- coding: utf-8 -*-
{
    'name': "Validación Masiva de Facturas AFIP",

    'summary': """
        Validación masiva de facturas en AFIP
        """,

    'description': """
        Validación masiva de facturas en AFIP. Una vez instalado, creará una
        acción en el menú acciones de Facturas.
    """,

    'author': "Alitux",
    'website': "https://delkarukinka.wordpress.com",

    'category': 'Invoicing',
    'version': '0.1',

    'depends': ['base','l10n_ar_afipws_fe'],

    'data': [
        'data/data.xml',
        'views/view_action.xml'
    ],

}
